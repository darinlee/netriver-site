package utils

import (
	"fmt"
	"gitee.com/darinlee/netriver-site/snmp"
)

func GetSlotNum(class int32) int32 {
	switch class {
	case snmp.CHASSIS_CLASS_2G1U3:
		return 3
	case snmp.CHASSIS_CLASS_2GBBU, snmp.CHASSIS_CLASS_2G1U4:
		return 4
	case snmp.CHASSIS_CLASS_2G1U6:
		return 6
	case snmp.CHASSIS_CLASS_2G2U8:
		return 8
	case snmp.CHASSIS_CHASS_3U12:
		return 12
	case snmp.CHASSIS_CLASS_4U16:
		return 16
	case snmp.CHASSIS_CLASS_4U17:
		return 17
	case snmp.CHASSIS_CLASS_5U20:
		return 20
	default:
		return 0
	}
}

var cwdmstr []string = []string{"COM", "PRO", "1331", "1271", "1351", "1291", "1371", "1311", "1531", "1471", "1551", "1491", "1571", "1511"}
var mwdmstr []string = []string{"COM", "PRO", "D26", "A27", "D28", "A29", "D30", "A31", "D32", "A33", "D34", "A35", "D36", "A37"}
func GetEntityName(unitClass, unitType, slot, port byte) string {
	switch unitClass {
	case snmp.UNIT_CLASS_OTU:
		if unitType > 10 && unitType < 15 {
			if port%2 != 0 {
				if unitType != 13 {
					return fmt.Sprintf("L%d", port/2+1)
				} else {
					return fmt.Sprintf("ONU%d", port/2+1)
				}
			} else {
				if unitType != 13 {
					return fmt.Sprintf("OLT%d", port/2)
				} else {
					return fmt.Sprintf("L%d", port/2)
				}
			}
		}
		if unitType == 100 {
			if port < 2 {
				return "C"
			} else {
				return fmt.Sprintf("L%d", port-1)
			}
		}
		if unitType == 110 {
			if port < 2 {
				return "C"
			} else {
				return "L"
			}
		}
		if unitType == 200 {
			if port != 2 {
				return fmt.Sprintf("C%d", port/2+1)
			} else {
				return "L"
			}
		}
		if unitType > 99 {
			if unitType < 200 {
				if port%2 != 0 {
					return fmt.Sprintf("L%d", port/2+1)
				} else {
					return fmt.Sprintf("C%d", port/2)
				}
			}
			if port == 1 {
				return "QSFP28-2"
			}
			if port == 2 {
				return "CFP2"
			}
			return "QSFP28-1"
		}
		if unitType == 6 { // BBU/RU OTU
			if port%2 != 0 {
				return fmt.Sprintf("L%d", (slot-1)*4+port/2+1)
			} else {
				return fmt.Sprintf("C%d", (slot-1)*4+port/2)
			}
		}
		if port%2 != 0 {
			return fmt.Sprintf("L%d", port/2+1)
		} else {
			return fmt.Sprintf("C%d", port/2)
		}
	case 11: //snmp.UNIT_CLASS_OEO:
		return fmt.Sprintf("C%d", port)
	case 12: //snmp.UNIT_CLASS_OTAP:
		if unitType <= 19 {
			break
		}
		if port < 2 {
			return "A组光口"
		} else {
			return "B组光口"
		}
	case 13: //snmp.UNIT_CLASS_OTU32Port:
		if port < 17 {
			return fmt.Sprintf("C%d", port)
		}
		number := 32 - port
		if number%2 != 1 {
			number += 2
		}
		return fmt.Sprintf("L%d", number)
	case 14: //snmp.UNIT_CLASS_ETU:
		if port%2 != 0 {
			return fmt.Sprintf("LAN%d", port/2+1)
		} else {
			return fmt.Sprintf("SFP%d", port/2)
		}
	case snmp.UNIT_CLASS_OLPM:
		if !(unitType > 16 && unitType < 20 || unitType > 21 && unitType < 31) && port <= 0 {
			return ""
		}
		if port == 0 {
			return ""
		}
		if port < 20 {
			if port < 15 {
				if unitType == 24 || unitType == 29 {
					return mwdmstr[port-1]
				} else {
					return cwdmstr[port-1]
				}
			} else {
				if port%2 == 1 {
					return fmt.Sprintf("T%d", (port-1)/2)
				} else {
					return fmt.Sprintf("R%d", (port-2)/2)
				}
			}
		}
		if port < 30 {
			return (mwdmstr[(port-20)*2]+ "光模块")
		}
		if port < 40 {
			return (mwdmstr[(port-30)*2+1] + "光模块")
		}
	default:
		if port > 0 && port < 9 {
			return fmt.Sprintf("端口%d", port)
		}
		return ""
	}
	return ""
}
