package utils

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"strings"
	"strconv"
)

func Struct2Bytes(data interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)
	if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func Bytes2Struct(data []byte, result interface{}) error {
	buf := bytes.NewBuffer(data)
	return binary.Read(buf, binary.LittleEndian, result)
}

func ParseOctetString(data interface{}) []byte {
	if result, ok := data.([]byte); ok {
		return result
	}
	if result, ok := data.(string); ok {
		return []byte(result)
	}
	return []byte{}
}

func StringIpToInt(ipString string) int {
	ipSegs := strings.Split(ipString, ".")
	var ipInt int32
	buf := []byte{}
	for _, ipSeg := range ipSegs {
		tempInt, _ := strconv.Atoi(ipSeg)
		buf = append(buf, byte(tempInt))
	}
	val := bytes.NewReader(buf)
	err := binary.Read(val, binary.BigEndian, &ipInt)
	if err != nil {
		fmt.Println(err)
	}
	return int(ipInt)
}
