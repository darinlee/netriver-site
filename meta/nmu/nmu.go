package nmu

// NMU端口
type NMUPort struct {
	SFP1On    uint8 `json:"sfp1On"`    // SFP1模块在位状态 1-在位 2-脱位
	SFP1State uint8 `json:"sfp1State"` // SFP1状态 1-Link 2-NoLink
	SFP2On    uint8 `json:"sfp2On"`    // SFP2模块在位状态 1-在位 2-脱位
	SFP2State uint8 `json:"sfp2State"` // SFP2状态 1-Link 2-NoLink
	LAN1State uint8 `json:"lan1State"` // LAN1状态 1-Link 2-NoLink
	LAN2State uint8 `json:"lan2State"` // LAN2状态 1-Link 2-NoLink
	SFP3On    uint8 `json:"sfp3On"`    // SFP3模块在位状态 1-在位 2-脱位
	SFP3State uint8 `json:"sfp3State"` // SFP3状态 1-Link 2-NoLink
	Power1Led uint8 `json:"power1Led"`
	Power2Led uint8 `json:"power2Led"`
	FanLed 	  uint8 `json:"fanLed"`
}
