package ddm

type DDM struct {
	TX1          int16  `json:"tx1"`          // 光口1接收光功率
	RX1          int16  `json:"rx1"`          // 光口1发送光功率
	Voltage1     uint16 `json:"voltage1"`     // 光口1电压
	BiasCurrent1 uint16 `json:"biasCurrent1"` // 光口1偏置电流
	Temperature1 int16  `json:"temperature1"` // 光口1温度
	TX2          int16  `json:"tx2"`          // 光口2接收光功率
	RX2          int16  `json:"rx2"`          // 光口2发送光功率
	Voltage2     uint16 `json:"voltage2"`     // 光口2电压
	BiasCurrent2 uint16 `json:"biasCurrent2"` // 光口2偏置电流
	Temperature2 int16  `json:"temperature2"` // 光口2温度
	TX3          int16  `json:"tx3"`          // 光口3接收光功率
	RX3          int16  `json:"rx3"`          // 光口3发送光功率
	Voltage3     uint16 `json:"voltage3"`     // 光口3电压
	BiasCurrent3 uint16 `json:"biasCurrent3"` // 光口3偏置电流
	Temperature3 int16  `json:"temperature3"` // 光口3温度
	TX4          int16  `json:"rx4"`          // 光口4接收光功率
	RX4          int16  `json:"tx4"`          // 光口4发送光功率
	Voltage4     uint16 `json:"voltage4"`     // 光口4电压
	BiasCurrent4 uint16 `json:"biasCurrent4"` // 光口4偏置电流
	Temperature4 int16  `json:"temperature4"` // 光口4温度
}
