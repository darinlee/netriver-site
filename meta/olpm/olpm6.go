package olpm

// OLP-M-6端口/线路状态
type OLPM6Port struct {
	COM1       int16 		`json:"com1"`		// COM1光功率
	COM2       int16 		`json:"com2"`		// COM2光功率
	T1         int16 		`json:"t1"`			// T1光功率
	R1         int16 		`json:"r1"`			// R1光功率
	T2         int16 		`json:"t2"`			// T2光功率
	R2         int16 		`json:"r2"`			// R2光功率
	T3         int16 		`json:"t3"`			// T3光功率
	R3         int16 		`json:"r3"`			// R3光功率
	L1         int16  		`json:"l1"`			// L1切换功率
	L2         int16  		`json:"l2"`			// L2切换功率
	ReturnTime uint16 		`json:"returnTime"`	// 返回时间
	WorkMode   uint8  		`json:"workMode"`	// 工作模式 1-自动 2-手动
	Line       uint8  		`json:"line"`		// 线路选择 1-主路 2-备路
	ReturnMode uint8  		`json:"returnMode"`	// 返回模式 1-开启 2-关闭
	COM1Light  uint8  		`json:"com1Light"`	// COM1灯状态
	COM2Light  uint8  		`json:"com2Light"`	// COM2灯状态
	T1Light    uint8  		`json:"t1Light"`	// T1灯状态
	R1Light    uint8  		`json:"r1Light"`	// R1灯状态
	T2Light    uint8  		`json:"t2Light"`	// T2灯状态
	R2Light    uint8  		`json:"r2Light"`	// R2灯状态
	T3Light    uint8  		`json:"t3Light"`	// T3灯状态
	R3Light    uint8  		`json:"r3Light"`	// R3灯状态
	Reserve    uint8  		`json:"reserve"`	// 预留
	Temp 	   int16 		`json:"temp"`		//温度
	AlmStatus  uint8		`json:"almStatus"`	//告警灯状态
	RunStatus  uint8		`json:"runStatus"`	//运行灯状态
}