package olpm

// OLP-M-18端口/线路状态1 (65, 1, 36)
type OLPM18Port1 struct {
	COM1 uint16 // COM1光功率
	COM2 uint16 // COM2光功率
	T1   uint16 // T1光功率
	R1   uint16 // R1光功率
	T2   uint16 // T2光功率
	R2   uint16 // R2光功率
	T3   uint16 // T3光功率
	R3   uint16 // R3光功率
	T4   uint16 // T4光功率
	R4   uint16 // R4光功率
	T5   uint16 // T5光功率
	R5   uint16 // R5光功率
	T6   uint16 // T6光功率
	R6   uint16 // R6光功率
	T7   uint16 // T7光功率
	R7   uint16 // R7光功率
	T8   uint16 // T8光功率
	R8   uint16 // R8光功率
	T9   uint16 // T9光功率
	R9   uint16 // R9光功率
}

// OLP-M-18端口/线路状态1 (66, 1, 34)
type OLPM18Port2 struct {
	L1         int16  // L1切换功率
	L2         int16  // L2切换功率
	ReturnTime uint16 // 返回时间
	WorkMode   uint8  // 工作模式 1-自动绿 2-手动红
	Line       uint8  // 线路选择 1-主路绿 2-备路红
	ReturnMode uint8  // 返回模式 1-开启绿 2-关闭红
	COM1Light  uint8  // COM1光功率 1-正常绿 2-不正常灭
	COM2Light  uint8  // COM2光功率 1-正常绿 2-不正常灭
	T1Light    uint8  // T1灯状态 1-正常绿 2-不正常灭
	R1Light    uint8  // R1灯状态 1-正常绿 2-不正常灭
	T2Light    uint8  // T2灯状态 1-正常绿 2-不正常灭
	R2Light    uint8  // R2灯状态 1-正常绿 2-不正常灭
	T3Light    uint8  // T3灯状态 1-正常绿 2-不正常灭
	R3Light    uint8  // R3灯状态 1-正常绿 2-不正常灭
	T4Light    uint8  // T4灯状态 1-正常绿 2-不正常灭
	R4Light    uint8  // R4灯状态 1-正常绿 2-不正常灭
	T5Light    uint8  // T5灯状态 1-正常绿 2-不正常灭
	R5Light    uint8  // R5灯状态 1-正常绿 2-不正常灭
	T6Light    uint8  // T6灯状态 1-正常绿 2-不正常灭
	R6Light    uint8  // R7灯状态 1-正常绿 2-不正常灭
	T7Light    uint8  // T4灯状态 1-正常绿 2-不正常灭
	R7Light    uint8  // R4灯状态 1-正常绿 2-不正常灭
	T8Light    uint8  // T5灯状态 1-正常绿 2-不正常灭
	R8Light    uint8  // R5灯状态 1-正常绿 2-不正常灭
	T9Light    uint8  // T6灯状态 1-正常绿 2-不正常灭
	R9Light    uint8  // R7灯状态 1-正常绿 2-不正常灭
	Reserve    uint8  // 预留
	COM1       uint16 // COM1光功率
	COM2       uint16 // COM2光功率
}
