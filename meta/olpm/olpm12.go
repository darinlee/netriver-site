package olpm

// OLP-M-12端口/线路状态1 (65, 1, 28)
type OLPM12Port1 struct {
	COM1 int16 `json:"com1"` // COM1光功率
	COM2 int16 `json:"com2"` // COM2光功率
	T1   int16 `json:"t1"`   // T1光功率
	R1   int16 `json:"r1"`   // R1光功率
	T2   int16 `json:"t2"`   // T2光功率
	R2   int16 `json:"r2"`   // R2光功率
	T3   int16 `json:"t3"`   // T3光功率
	R3   int16 `json:"r3"`   // R3光功率
	T4   int16 `json:"t4"`   // T4光功率
	R4   int16 `json:"r4"`   // R4光功率
	T5   int16 `json:"t5"`   // T5光功率
	R5   int16 `json:"r5"`   // R5光功率
	T6   int16 `json:"t6"`   // T6光功率
	R6   int16 `json:"r6"`   // R6光功率
	Temp int16 	`json:"temp"` //温度
}

// OLP-M-12端口/线路状态1 (66, 1, 18)
type OLPM12Port2 struct {
	L1         int16  `json:"l1"`         // L1切换功率
	L2         int16  `json:"l2"`         // L2切换功率
	ReturnTime uint16 `json:"returnTime"` // 返回时间
	WorkMode   uint8  `json:"workMode"`   // 工作模式 1-自动绿 2-手动红
	Line       uint8  `json:"line"`       // 线路选择 1-主路绿 2-备路红
	ReturnMode uint8  `json:"returnMode"` // 返回模式 1-开启绿 2-关闭红
	COM1Light  uint8  `json:"com1Light"`  // COM1光功率 1-正常绿 2-不正常灭
	COM2Light  uint8  `json:"com2Light"`  // COM2光功率 1-正常绿 2-不正常灭
	T1Light    uint8  `json:"t1Light"`    // T1灯状态 1-正常绿 2-不正常灭
	R1Light    uint8  `json:"r1Light"`    // R1灯状态 1-正常绿 2-不正常灭
	T2Light    uint8  `json:"t2Light"`    // T2灯状态 1-正常绿 2-不正常灭
	R2Light    uint8  `json:"r2Light"`    // R2灯状态 1-正常绿 2-不正常灭
	T3Light    uint8  `json:"t3Light"`    // T3灯状态 1-正常绿 2-不正常灭
	R3Light    uint8  `json:"r3Light"`    // R3灯状态 1-正常绿 2-不正常灭
	T4Light    uint8  `json:"t4Light"`    // T4灯状态 1-正常绿 2-不正常灭
	R4Light    uint8  `json:"r4Light"`    // R4灯状态 1-正常绿 2-不正常灭
	T5Light    uint8  `json:"t5Light"`    // T5灯状态 1-正常绿 2-不正常灭
	R5Light    uint8  `json:"r5Light"`    // R5灯状态 1-正常绿 2-不正常灭
	T6Light    uint8  `json:"t6Light"`    // T6灯状态 1-正常绿 2-不正常灭
	R6Light    uint8  `json:"r6Light"`    // R7灯状态 1-正常绿 2-不正常灭
	Reserve    uint8  `json:"reserve"`    // 预留
	AlmStatus  uint8  `json:"almStatus"`	//告警灯状态
	RunStatus  uint8  `json:"runStatus"`	//运行灯状态
}

type OLPM12OAM1 struct {
	T1R           uint16 `json:"t1r"`           // T1局端接收光功率
	T1S           uint16 `json:"t1s"`           // T1局端发送光功率
	T1Temperature uint16 `json:"t1Temperature"` // T1局端温度
	T1BiasCurrent uint16 `json:"t1BiasCurrent"` // T1局端偏置电流
	T1Voltage     uint16 `json:"t1Voltage"`     // T1局端电压
	R1R           uint16 `json:"r1r"`           // R1远端接收光功率
	R1S           uint16 `json:"r1s"`           // R1远端发送光功率
	R1Temperature uint16 `json:"r1Temperature"` // R1远端温度
	R1BiasCurrent uint16 `json:"r1BiasCurrent"` // R1远端偏置电流
	R1Voltage     uint16 `json:"r1Voltage"`     // R1远端电压
	T2R           uint16 `json:"t2r"`           // T2局端接收光功率
	T2S           uint16 `json:"t2s"`           // T2局端发送光功率
	T2Temperature uint16 `json:"t2Temperature"` // T2局端温度
	T2BiasCurrent uint16 `json:"t2BiasCurrent"` // T2局端偏置电流
	T2Voltage     uint16 `json:"t2Voltage"`     // T2局端电压
	R2R           uint16 `json:"r2r"`           // R2远端接收光功率
	R2S           uint16 `json:"r2s"`           // R2远端发送光功率
	R2Temperature uint16 `json:"r2Temperature"` // R2远端温度
	R2BiasCurrent uint16 `json:"r2BiasCurrent"` // R2远端偏置电流
	R2Voltage     uint16 `json:"r2Voltage"`     // R2远端电压
}

type OLPM12OAM2 struct {
	T3R           uint16 `json:"t3r"`           // T3局端接收光功率
	T3S           uint16 `json:"t3s"`           // T3局端发送光功率
	T3Temperature uint16 `json:"t3Temperature"` // T3局端温度
	T3BiasCurrent uint16 `json:"t3BiasCurrent"` // T3局端偏置电流
	T3Voltage     uint16 `json:"t3Voltage"`     // T3局端电压
	R3R           uint16 `json:"r3r"`           // R3远端接收光功率
	R3S           uint16 `json:"r3s"`           // R3远端发送光功率
	R3Temperature uint16 `json:"r3Temperature"` // R3远端温度
	R3BiasCurrent uint16 `json:"r3BiasCurrent"` // R3远端偏置电流
	R3Voltage     uint16 `json:"r3Voltage"`     // R3远端电压
	T4R           uint16 `json:"t4r"`           // T4局端接收光功率
	T4S           uint16 `json:"t4s"`           // T4局端发送光功率
	T4Temperature uint16 `json:"t4Temperature"` // T4局端温度
	T4BiasCurrent uint16 `json:"t4BiasCurrent"` // T4局端偏置电流
	T4Voltage     uint16 `json:"t4Voltage"`     // T4局端电压
	R4R           uint16 `json:"r4r"`           // R4远端接收光功率
	R4S           uint16 `json:"r4s"`           // R4远端发送光功率
	R4Temperature uint16 `json:"r4Temperature"` // R4远端温度
	R4BiasCurrent uint16 `json:"r4BiasCurrent"` // R4远端偏置电流
	R4Voltage     uint16 `json:"r4Voltage"`     // R4远端电压
}

type OLPM12OAM3 struct {
	T5R           uint16 `json:"t5r"`           // T5局端接收光功率
	T5S           uint16 `json:"t5s"`           // T5局端发送光功率
	T5Temperature uint16 `json:"t5Temperature"` // T5局端温度
	T5BiasCurrent uint16 `json:"t5BiasCurrent"` // T5局端偏置电流
	T5Voltage     uint16 `json:"t5Voltage"`     // T5局端电压
	R5R           uint16 `json:"r5r"`           // R5远端接收光功率
	R5S           uint16 `json:"r5s"`           // R5远端发送光功率
	R5Temperature uint16 `json:"r5Temperature"` // R5远端温度
	R5BiasCurrent uint16 `json:"r5BiasCurrent"` // R5远端偏置电流
	R5Voltage     uint16 `json:"r5Voltage"`     // R5远端电压
	T6R           uint16 `json:"t6r"`           // T6局端接收光功率
	T6S           uint16 `json:"t6s"`           // T6局端发送光功率
	T6Temperature uint16 `json:"t6Temperature"` // T6局端温度
	T6BiasCurrent uint16 `json:"t6BiasCurrent"` // T6局端偏置电流
	T6Voltage     uint16 `json:"t6Voltage"`     // T6局端电压
	R6R           uint16 `json:"r6r"`           // R6远端接收光功率
	R6S           uint16 `json:"r6s"`           // R6远端发送光功率
	R6Temperature uint16 `json:"r6Temperature"` // R6远端温度
	R6BiasCurrent uint16 `json:"r6BiasCurrent"` // R6远端偏置电流
	R6Voltage     uint16 `json:"r6Voltage"`     // R6远端电压
}

type OLPM12OAM4 struct {
	ModelID   [12]byte `json:"modelId"`   // 模块ID(对应波长)
	Los       [12]byte `json:"los"`       // 模块Los状态 1-有光 2-无光
	FrameSync [12]byte `json:"frameSync"` // 帧同步状态 1-同步 2-失步
}

type OLPM12OAMT1 struct {
	T1Speed            uint8     `json:"t1Speed"`            // T1局端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	T1ManufacturerName [16]byte  `json:"t1ManufacturerName"` // T1局端模块供应商名称
	T1ManufacturerPN   [16]byte  `json:"t1ManufacturerPn"`   // T1局端模块供应商PN
	T1ManufacturerSN   [16]byte  `json:"t1ManufacturerSn"`   // T1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMR1 struct {
	R1Speed            uint8     `json:"r1Speed"`            // R1远端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	R1ManufacturerName [16]byte  `json:"r1ManufacturerName"` // R1局端模块供应商名称
	R1ManufacturerPN   [16]byte  `json:"r1ManufacturerPn"`   // R1局端模块供应商PN
	R1ManufacturerSN   [16]byte  `json:"r1ManufacturerSn"`   // R1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMT2 struct {
	T1Speed            uint8     `json:"t1Speed"`            // T1局端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	T1ManufacturerName [16]byte  `json:"t1ManufacturerName"` // T1局端模块供应商名称
	T1ManufacturerPN   [16]byte  `json:"t1ManufacturerPn"`   // T1局端模块供应商PN
	T1ManufacturerSN   [16]byte  `json:"t1ManufacturerSn"`   // T1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMR2 struct {
	R1Speed            uint8     `json:"r1Speed"`            // R1远端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	R1ManufacturerName [16]byte  `json:"r1ManufacturerName"` // R1局端模块供应商名称
	R1ManufacturerPN   [16]byte  `json:"r1ManufacturerPn"`   // R1局端模块供应商PN
	R1ManufacturerSN   [16]byte  `json:"r1ManufacturerSn"`   // R1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMT3 struct {
	T1Speed            uint8     `json:"t1Speed"`            // T1局端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	T1ManufacturerName [16]byte  `json:"t1ManufacturerName"` // T1局端模块供应商名称
	T1ManufacturerPN   [16]byte  `json:"t1ManufacturerPn"`   // T1局端模块供应商PN
	T1ManufacturerSN   [16]byte  `json:"t1ManufacturerSn"`   // T1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMR3 struct {
	R1Speed            uint8     `json:"r1Speed"`            // R1远端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	R1ManufacturerName [16]byte  `json:"r1ManufacturerName"` // R1局端模块供应商名称
	R1ManufacturerPN   [16]byte  `json:"r1ManufacturerPn"`   // R1局端模块供应商PN
	R1ManufacturerSN   [16]byte  `json:"r1ManufacturerSn"`   // R1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMT4 struct {
	T1Speed            uint8     `json:"t1Speed"`            // T1局端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	T1ManufacturerName [16]byte  `json:"t1ManufacturerName"` // T1局端模块供应商名称
	T1ManufacturerPN   [16]byte  `json:"t1ManufacturerPn"`   // T1局端模块供应商PN
	T1ManufacturerSN   [16]byte  `json:"t1ManufacturerSn"`   // T1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMR4 struct {
	R1Speed            uint8     `json:"r1Speed"`            // R1远端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	R1ManufacturerName [16]byte  `json:"r1ManufacturerName"` // R1局端模块供应商名称
	R1ManufacturerPN   [16]byte  `json:"r1ManufacturerPn"`   // R1局端模块供应商PN
	R1ManufacturerSN   [16]byte  `json:"r1ManufacturerSn"`   // R1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMT5 struct {
	T1Speed            uint8     `json:"t1Speed"`            // T1局端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	T1ManufacturerName [16]byte  `json:"t1ManufacturerName"` // T1局端模块供应商名称
	T1ManufacturerPN   [16]byte  `json:"t1ManufacturerPn"`   // T1局端模块供应商PN
	T1ManufacturerSN   [16]byte  `json:"t1ManufacturerSn"`   // T1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}

type OLPM12OAMR5 struct {
	R1Speed            uint8     `json:"r1Speed"`            // R1远端速率 1-10G 2-25G
	Reserve1           [7]byte   `json:"reserve1"`           // 保留
	R1ManufacturerName [16]byte  `json:"r1ManufacturerName"` // R1局端模块供应商名称
	R1ManufacturerPN   [16]byte  `json:"r1ManufacturerPn"`   // R1局端模块供应商PN
	R1ManufacturerSN   [16]byte  `json:"r1ManufacturerSn"`   // R1局端模块供应商SN
	Reserve2           [288]byte `json:"reserve2"`           // 保留
}