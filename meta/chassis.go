package meta

// 系统信息（电源、风扇、温度）状态
type ChassisInfo struct {
	Temperature    int16  `json:"temperature"`    // 机箱温度
	Power1OnOff    uint8  `json:"power1OnOff"`    // 电源1关断状态 1-PowerOn 2-PowerOff
	Power1Type     uint8  `json:"power1Type"`     // 电源1类型 1-AC48 2-DC220
	Power1Voltage  int16  `json:"power1Voltage"`   // 电源1当前电压
	Power1FanState uint8  `json:"power1FanState"` // 电源1风扇状态 1-Running 2-Off
	Power1Exist    uint8  `json:"power1Exist"`    // 电源1存在状态 1-Exist 2-NotExist
	Power2OnOff    uint8  `json:"power2OnOff"`    // 电源2关断状态 1-PowerOn 2-PowerOff
	Power2Type     uint8  `json:"power2Type"`     // 电源2类型 1-AC48 2-DC220
	Power2Voltage  int16  `json:"power2Voltage"`   // 电源2当前电压
	Power2FanState uint8  `json:"power2FanState"` // 电源2风扇状态 1-Running 2-Off
	Power2Exist    uint8  `json:"power2Exist"`    // 电源2存在状态 1-Exist 2-NotExist
	Fan1Speed      uint16 `json:"fan1Speed"`      // 风扇1转速
	Fan1State      uint8  `json:"fan1State"`      // 风扇1状态 1-Running 2-Off
	Reserve1       uint8  `json:"reserve1"`       // 预留
	Fan2Speed      uint16 `json:"fan2Speed"`      // 风扇2转速
	Fan2State      uint8  `json:"fan2State"`      // 风扇2状态 1-Running 2-Off
	Reserve2       uint8  `json:"reserve2"`       // 预留
	Fan3Speed      uint16 `json:"fan3Speed"`      // 风扇3转速
	Fan3State      uint8  `json:"fan3State"`      // 风扇3状态 1-Running 2-Off
	Reserve3       uint8  `json:"reserve3"`       // 预留
	Fan4Speed      uint16 `json:"fan4Speed"`      // 风扇4转速
	Fan4State      uint8  `json:"fan4State"`      // 风扇4状态 1-Running 2-Off
	Reserve4       uint8  `json:"reserve4"`       // 预留
	Power1State    uint8  `json:"power1State"`    // 电源1状态 1-PowerOn 2-PowerOff
	Power2State    uint8  `json:"power2State"`    // 电源2状态 1-PowerOn 2-PowerOff
	FanLED         uint8  `json:"fanLED"`         // 风扇LED 1-正常 2-错误
	Reserve5       uint8  `json:"reserve5"`       // 预留
}

// 系统信息（电源、风扇、温度）告警阈值
type ChassisThreshold struct {
	ChassisMinTemperature int8   `json:"chassisMinTemperature"` // 机箱最小温度
	ChassisMaxTemperature int8   `json:"chassisMaxTemperature"` // 机箱最大温度
	PowerMinVoltage       uint8  `json:"powerMinVoltage"`       // 电源最小电压
	PowerMaxVoltage       uint8  `json:"powerMaxVoltage"`       // 电源最大电压
	ChassisFanMinSpeed    uint16 `json:"chassisFanMinSpeed"`    // 机箱风扇最小转速
	ChassisFanMaxSpeed    uint16 `json:"chassisFanMaxSpeed"`    // 机箱风扇最大转速
	PowerFanMinSpeed      uint16 `json:"powerFanMinSpeed"`      // 电源风扇最小转速
	PowerFanMaxSpeed      uint16 `json:"powerFanMaxSpeed"`      // 电源风扇最大转速
}
