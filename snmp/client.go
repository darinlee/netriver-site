package snmp

import (
	"time"

	"github.com/gosnmp/gosnmp"
)

type Client struct {
	Target string
	Port   uint16
	Community string
}

func NewClient(target string, port uint16, community string) *Client {
	return &Client{Target: target, Port: port, Community: community}
}

func (c *Client) newSNMP() *gosnmp.GoSNMP {
	return &gosnmp.GoSNMP{
		Target:             c.Target,
		Port:               c.Port,
		Transport:          "udp",
		Community:          c.Community,
		Version:            gosnmp.Version2c,
		Timeout:            5 * time.Second,
		Retries:            3,
		ExponentialTimeout: true,
		MaxOids:            60,
	}
}

func (c *Client) CheckConnectivity() error {
	cli := c.newSNMP()
	err := cli.Connect()
	if err != nil {
		return err
	}
	defer cli.Conn.Close()
	return nil
}

func (c *Client) SNMPGet(oids []string) (*gosnmp.SnmpPacket, error) {
	cli := c.newSNMP()
	err := cli.Connect()
	if err != nil {
		return nil, err
	}
	defer cli.Conn.Close()
	pac, err := cli.Get(oids)
	if err != nil {
		return nil, err
	}
	return pac, nil
}

func (c *Client) SNMPSet(pdus []gosnmp.SnmpPDU) (*gosnmp.SnmpPacket, error) {
	cli := c.newSNMP()
	err := cli.Connect()
	if err != nil {
		return nil, err
	}
	defer cli.Conn.Close()
	pac, err := cli.Set(pdus)
	if err != nil {
		return nil, err
	}
	return pac, nil
}

func (c *Client) SNMPWalkAll(oids string) ([]gosnmp.SnmpPDU, error) {
	cli := c.newSNMP()
	err := cli.Connect()
	if err != nil {
		return nil, err
	}
	defer cli.Conn.Close()
	res, err := cli.WalkAll(oids)
	if err != nil {
		return nil, err
	}
	return res, nil
}