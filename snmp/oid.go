package snmp

// INFOSET 信息集ID
const (
	INFOSET_CHASSIS       = 3
	INFOSET_SYS_THRESHOLD = 4
	INFOSET_NMU           = 8
	INFOSET_DDM           = 11
	INFOSET_DDM_THRESHOLD = 12
	INFOSET_OLPM6         = 65
	INFOSET_OLPM12_1      = 65
	INFOSET_OLPM12_2      = 66
	INFOSET_OLPM18_1      = 65
	INFOSET_OLPM18_2      = 66
)

// CHASSIS_CLASS 机箱大类
const (
	CHASSIS_CLASS_NONE  = 0
	CHASSIS_CLASS_2G1U3 = 1
	CHASSIS_CLASS_2G1U4 = 11
	CHASSIS_CLASS_2GBBU = 12
	CHASSIS_CLASS_2G2U8 = 120
	CHASSIS_CLASS_2G1U6 = 111

	CHASSIS_CHASS_3U12 = 31
	CHASSIS_CLASS_4U16 = 4
	CHASSIS_CLASS_4U17 = 41
	CHASSIS_CLASS_5U20 = 51
)

// UNIT_CLASS 单元盘大类
const (
	UNIT_CLASS_NONE = 0
	UNIT_CLASS_NMU  = 1
	UNIT_CLASS_OTU  = 3
	UNIT_CLASS_OLPM = 4
	UNIT_CLASS_EDFA = 9
)

// NMU_TYPE NMU类型
const (
	NMU_TYPE_NONE byte = 0
	NMU_TYPE      byte = 1
)

// OLPM_TYPE OLPM类型
const (
	OLPM_TYPE_NONE = 0
	OLPM_TYPE_6    = 18
	OLPM_TYPE_12   = 19
)

// OID
const (
	OID_UNIT_CLASS        = ".1.3.6.1.4.1.10215.2.1.5.88.1.1.1.1.%d"        // 单元盘大类: UnitNum
	OID_UNIT_TYPE         = ".1.3.6.1.4.1.10215.2.1.5.88.1.1.1.2.%d"        // 单元盘小类: UnitNum
	OID_INFOSET           = ".1.3.6.1.4.1.10215.2.1.5.88.1.4.1.5.0"         // 信息集: Slot,InfoSet,Index
	OID_HARDWARE_VERSION  = ".1.3.6.1.4.1.10215.2.1.2.1.2.1.1.2.0.%d"       // 硬件版本号: Slot
	OID_SOFTWARE_VERSION  = ".1.3.6.1.4.1.10215.2.1.2.1.2.1.1.3.0.%d"       // 软件版本号: Slot
	OID_MODEL             = ".1.3.6.1.4.1.10215.2.1.2.1.2.1.1.4.0.%d"       // 型号: Slot
	OID_SN                = ".1.3.6.1.4.1.10215.2.1.2.1.2.1.1.5.0.%d"       // SN: Slot
	OID_MANUAFCTURED_DATE = ".1.3.6.1.4.1.10215.2.1.2.1.2.1.1.6.0.%d"       // 生产日期: Slot
	OID_NETWORK           = ".1.3.6.1.4.1.10215.2.1.2.1.2.3.1.1.%d.0.%d"    // 网络信息
	OID_DESTHOST          = ".1.3.6.1.4.1.10215.2.1.9.1.1"                  // snmp目标主机
	OID_FACTORY           = ".1.3.6.1.4.1.10215.2.1.2.1.3.1.2.1.%d.0.%d.%d" // 出厂信息
)
